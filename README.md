# Kubectl Envsubst

![https://gitlab.com/gimse/kubectl-envsubst/-/pipelines/main/latest](https://gitlab.com/gimse/kubectl-envsubst/badges/main/pipeline.svg?key_text=main%20pipeline&key_width=80)

## Getting Started with Docker

- Install Docker

- `docker build -t kubectl-ensubst .`

## Push to Dockerhub

- Setup file variable [DOCKERHUB_CONFIG](https://gitlab.com/gimse/kubectl-envsubst/-/settings/ci_cd) as file desribed at https://github.com/GoogleContainerTools/kaniko#pushing-to-docker-hub


## How to Setup Manifest

dckr_pat_YE8U1JHt0I8n1_aE089-zzCPmFM

docker pull docker.io/gimse/kubectl-envsubst:1.27.4-amd64

docker pull docker.io/gimse/kubectl-envsubst:1.27.4-arm

docker manifest create gimse/kubectl-envsubst:1.27.4 "gimse/kubectl-envsubst:1.27.4-amd64" "gimse/kubectl-envsubst:1.27.4-arm"


docker manifest push --purge gimse/kubectl-envsubst:1.27.4