FROM registry.hub.docker.com/library/ubuntu:22.10

RUN apt-get update && apt-get install curl gettext-base -y

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

ENTRYPOINT ["/bin/bash"]